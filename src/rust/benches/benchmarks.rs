use criterion::{black_box, criterion_group, criterion_main, Criterion};
use rand::Rng;
use itertools::Itertools;
use std::iter;
use criterion::BenchmarkId;
use criterion::Throughput;
use bektas_utils_r::*;
use std::time::Duration;
   

fn bench_get_dist_matrix(c: &mut Criterion) {
    // let n = 100;
    // let xpos: Vec<i32> = (1..n).into_iter().map(|_| rand::thread_rng().gen_range(0..(2*n))).collect();
    // let xpos: Vec<i32> = xpos.into_iter().unique().collect();
    // let x: Vec<f64> = (0..(xpos.len())).into_iter().map(|_| rand::thread_rng().gen_range(0.0..2.0)).collect();
    // c.bench_function("get_dist_matrix", |b| b.iter(|| get_dist_matrix(black_box(&xpos), black_box(&xpos), n/2, "euclidean", 0)));

    let mut group = c.benchmark_group("scanning n for get_dist_matrix");
    // group.measurement_time(Duration::from_secs(20));
    group.sample_size(10);
    for i in (2..5).into_iter() {
        let n = (10 as i32).pow(i);
        let xpos: Vec<i32> = (1..n).into_iter().map(|_| rand::thread_rng().gen_range(0..(2*n))).collect();
        let xpos: Vec<i32> = xpos.into_iter().unique().collect();
        group.bench_with_input(BenchmarkId::from_parameter(i), &i, |b, &i| {
            b.iter(|| get_dist_matrix(black_box(&xpos), black_box(&xpos), n/2, "euclidean", 0)
            );
        });
    }
    group.finish();
}


fn bench_conn_corr_inhomo(c: &mut Criterion) {
    // let n = 100;
    // let xpos: Vec<i32> = (1..n).into_iter().map(|_| rand::thread_rng().gen_range(0..(2*n))).collect();
    // let xpos: Vec<i32> = xpos.into_iter().unique().collect();
    // let x: Vec<f64> = (0..(xpos.len())).into_iter().map(|_| rand::thread_rng().gen_range(0.0..2.0)).collect();
    // c.bench_function("get_dist_matrix", |b| b.iter(|| get_dist_matrix(black_box(&xpos), black_box(&xpos), n/2, "euclidean", 0)));

    let mut group = c.benchmark_group("scanning n for conn_corr_inhomo");
    // group.measurement_time(Duration::from_secs(20));
    group.sample_size(3);

    for i in (2..5).into_iter() {
        let n = (10 as i32).pow(i);
        let xpos: Vec<i32> = (1..n).into_iter().map(|_| rand::thread_rng().gen_range(0..(2*n))).collect();
        let xpos: Vec<i32> = xpos.into_iter().unique().collect();
        let x: Vec<f64> = (0..(xpos.len())).into_iter().map(|_| rand::thread_rng().gen_range(0.0..2.0)).collect();
        group.bench_with_input(BenchmarkId::from_parameter(i), &i, |b, &i| {
            b.iter(|| conn_crosscor_inhomo(black_box(&x), black_box(&x), black_box(&xpos), black_box(&xpos), n/2, "euclidean", 0)
            );
        });
    }
    group.finish();
}


criterion_group!(benches, bench_get_dist_matrix, bench_conn_corr_inhomo);
criterion_main!(benches);
