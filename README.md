# Personal utility functions 

They are typically written in Rust/C++/Julia/R and called directly from R.

# How to install

```
install.packages("devtools")
devtools::install_git("https://gitlab.com/onurcanbektas/bektas.utils.r.git")
```

# How to use

After loading the package, you can use, for example

```
library(bektas.utils.r)
help(get_conn_crosscor_inhomo, package="bektas.utils.r")
```

to take a look at the documentation of the individual functions.

# How to benchmark

See the file `benchmark/benchmark.R` file.

