use extendr_api::prelude::*;
use rayon::prelude::*;

// Macro to generate exports.
// This ensures exported functions are registered with R.
// See corresponding C code in `entrypoint.c`.
extendr_module! {
    mod bektas_utils_r;
    fn Rpeuclidean;
    fn Rpeuclidean_pairwise;
    fn get_conn_crosscor_inhomo;
    fn conn_crosscor_inhomo_ordered;
    fn conn_crosscor_inhomo_nonordered;
    fn Rget_dist_matrix;
    fn get_density_field;
    fn get_density_grad;
    fn get_velocity_field;
    fn rget_local_environment;
}

#[extendr]
pub trait NumericOps {
    fn rem_euclid(self, rhs: Self) -> Self;
    fn min_op(self, other: Self) -> Self;
    fn abs(self) -> Self;
    fn get_binary(self) -> Self;
}
impl NumericOps for f64 {
    fn rem_euclid(self, rhs: Self) -> Self {
        self.rem_euclid(rhs)
    }
    fn min_op(self, other: Self) -> Self {
        f64::min(self, other)
    }
    fn abs(self) -> Self {
        self.abs()
    }
    fn get_binary(self) -> Self {
        if self > 0.0 {
            return 1.0
        } else if self < 0.0 {
            return -1.0
        } else {
            return 0.0
        }
    }
}
impl NumericOps for i32 {
    fn rem_euclid(self, rhs: Self) -> Self {
        self.rem_euclid(rhs)
    }
    fn min_op(self, other: Self) -> Self {
       i32::min(self, other) 
    }
    fn abs(self) -> Self {
        self.abs()
    }
    fn get_binary(self) -> Self {
        if self > 0 {
            return 1
        } else if self < 0{
            return -1
        } else {
            return 0
        }
    }
}

/// @title Calculates the euclidean distance between two points on a circle of radisus l
///
/// @examples
/// let us make a generic example
///
/// ```{r}
/// library(bektas.utils.r)
/// l <- 100
/// x <- runif(10, min=0, max=l)
/// y <- runif(10, min=0, max=l)
/// get_peuclidean(x, y, l, F)
/// ```
///
/// @export
fn peuclidean<T>(x: &T, y: &T, l: &T, is_signed: bool) -> T 
where 
    T: NumericOps,
    T: Copy,
    T: std::ops::Sub<Output=T>,
    T: std::cmp::PartialOrd,
    T: std::ops::Mul<Output=T>
{
    let s1 = (*x - *y).abs();
    let s2: T = s1.rem_euclid(*l) as T;
    let s3 = NumericOps::min_op(s2, *l - s2);
    let ret = s3.abs() as T;
    if is_signed == true {
        // let sign_ret = ((((s2 < l - s2) as u32) as T) * 2.0 - 1.0) * (x-y).signum();
        let sign_ret: T = NumericOps::get_binary(*l - s2 - s2) * NumericOps::get_binary(*x-*y);
        return ret * sign_ret;
    } else {
        return ret;
    }
}

#[extendr]
fn Rpeuclidean(x: Robj, y: Robj, l: f64, is_signed: Robj) -> f64 {
    let i_x: Option<f64> = x.as_real();
    let i_y: Option<f64> = y.as_real();
    let signed: bool = is_signed.as_bool().unwrap();

    match i_x.zip(i_y) {
        Some((x, y)) => peuclidean(&x, &y, &l, signed),
        None => panic!("The function couldn't convert the Robj to real vectors, dude!"),
    }
}

/// @export
#[extendr]
fn Rpeuclidean_pairwise(x: Robj, y: Robj, l: f64, is_signed: Robj) -> Vec<f64> {
    let i_x: Option<Vec<f64>> = x.as_real_vector();
    let i_y: Option<Vec<f64>> = y.as_real_vector();
    let signed: bool = is_signed.as_bool().unwrap();

    match i_x.zip(i_y) {
        Some((s_x, s_y)) => {
            let ret = s_x.iter().zip(s_y.iter()).map(|(x,y)| peuclidean(x, y, &l, signed)).collect();
            return ret;
        },
        None => panic!("The function couldn't convert the Robj to real vectors, dude!"),
    }
}


fn connected_crosscorrelation(x: &[f64], y: &[f64], lag: f64) -> f64 {
    let lx = x.len() as f64;
    let ly = y.len() as f64;
    if lx != ly {
        panic!("Input vectors have different lengths, dude!");
    } else if (lag >= lx) | (lag >= ly) {
        panic!("The lag is larger than the size of the input vectors, dude!");
    }
    else {
        let x = &x[0..((lx-lag) as usize)];
        let y = &y[(((lag as usize)))..(ly as usize)];
        let lx = x.len() as f64;
        let ly = y.len() as f64;

        let mx = x.iter().sum::<f64>() / lx;
        let my = y.iter().sum::<f64>() / ly;
        let nx = x.iter().map(|val| val - mx).collect::<Vec<f64>>();
        let ny = y.iter().map(|val| val - my).collect::<Vec<f64>>();

        let sc = (nx.iter().map(|val| val*val).sum::<f64>() *
            ny.iter().map(|val| val*val).sum::<f64>()).sqrt() / lx;

        // let ret = (nx.iter().zip(ny.iter()).map(|(a,b)| a*b).sum::<f64>() / lx) / sc;
        let ret = (nx.iter().zip(ny).map(|(a,b)| a*b).sum::<f64>() / lx) / sc;

        return ret;
    }
}


/// @title The connected cross-correlation of two ordered vectors
///
/// @description At the moment, we assume that the @xpos and @ypos are ordered within themselves.
/// Here we use some r-to-rust interface to make sure that the values are passed to rust and
/// interpreted correctly.
///
/// @examples
/// To explain how to use the function, let us take the following example:
/// Let us suppose that we have a data of methylations along the DNA. Let @xpos 
/// indicate the positions of the CpG sites, and @x denote whether the given CpG is 
/// methylated or not.
/// ```{r eval=F}
/// library(bektas.utils.r)
/// library(data.table)
/// n  <- 100
/// xpos  <- sort(unique(as.integer(runif(n, min = 0, max = n*3))))
/// x  <- (rnorm(length(xpos), mean = 0, sd = 1))
/// dt  <- data.table::data.table(x = x, xpos = xpos)
/// setDT(dt)
/// metric  <- "euclidean"
/// l  <- n*3
/// get_conn_crosscor_inhomo(dt$x, dt$x, dt$xpos, dt$xpos, as.integer(n/2), "euclidean", as.integer(l))
/// ```
///
/// @param x a vector
/// @param y a vector
/// @param xpos a vector
/// @param ypos a vectors
/// @param max_dist is a scalar value
/// @param metric is a string, "euclidean" and "periodic" is possible, but the latter is not fully
/// implemented
/// @param l is an integer, ignored if metric is "euclidean"
///
/// @export
#[extendr]
fn get_conn_crosscor_inhomo(x: Robj, y: Robj, xpos: Robj, ypos: Robj, max_dist: Robj, metric: Robj, l: Robj) -> Vec<f64> {
    let x: Option<Vec<f64>> = x.as_real_vector();
    let y: Option<Vec<f64>> = y.as_real_vector();
    let xpos: Option<Vec<_>> = xpos.as_integer_vector();
    let ypos: Option<Vec<_>> = ypos.as_integer_vector();
    let max_dist = max_dist.as_integer();
    let metric = metric.as_str();
    let l = l.as_integer();

    match (&x, &y, &xpos, &ypos, max_dist, metric, l) {
        (Some(s_x), Some(s_y), Some(s_xpos), 
         Some(s_ypos), Some(s_max_dist), Some(s_metric), Some(s_l)) => 
            conn_crosscor_inhomo_ordered(s_x, s_y, s_xpos, s_ypos, s_max_dist, s_metric, s_l),
            (None, _, _, _, _, _, _) => panic!("The function couldn't convert x to real vectors, dude!"),
            (_, None, _, _, _, _, _) => panic!("The function couldn't convert y to real vectors, dude!"),
            (_, _, None, _, _, _, _) => panic!("The function couldn't convert xpos to integer vectors, dude!"),
            (_, _, _, None, _, _, _) => panic!("The function couldn't convert ypos to integer vectors, dude!"),
            (_, _, _, _, None, _, _) => panic!("The function couldn't convert max_dist to integer, dude!"),
            (_, _, _, _, _, None, _) => panic!("The function couldn't convert metric to string, dude!"),
            (_, _, _, _, _, _, None) => panic!("The function couldn't convert l to integer, dude!")
    }
}


/// @title The connected cross-correlation of two unordered vectors
///
/// @description Here we do not assume that @xpos or @ypos are ordered
///
/// @examples
/// To explain how to use the function, let us take the following example:
/// Let us suppose that we have a data of methylations along the DNA. Let @xpos 
/// indicate the positions of the CpG sites, and @x denote whether the given CpG is 
/// methylated or not.
/// ```{r eval=F}
/// library(bektas.utils.r)
/// library(data.table)
/// n  <- 100
/// xpos  <- unique(as.integer(runif(n, min = 0, max = n*3)))
/// x  <- (rnorm(length(xpos), mean = 0, sd = 1))
/// dt  <- data.table::data.table(x = x, xpos = xpos)
/// setDT(dt)
/// metric  <- "euclidean"
/// l  <- n*3
/// get_conn_crosscor_inhomo(dt$x, dt$x, dt$xpos, dt$xpos, as.integer(n/2), "euclidean", as.integer(l))
/// ```
///
/// @param x a vector
/// @param y a vector
/// @param xpos a vector
/// @param ypos a vectors
/// @param max_dist is a scalar value
/// @param metric is a string, "euclidean" and "periodic" is possible, but the latter is not fully
/// implemented
/// @param l is an integer, ignored if metric is "euclidean"
///
/// @export
#[extendr]
pub fn conn_crosscor_inhomo_nonordered(x: &[f64], y: &[f64], xpos: &[i32], ypos: &[i32], max_dist: i32, metric: &str, l: i32) -> Vec<f64> {
    let lx = x.len() as f64;
    let ly = y.len() as f64;
    let lxpos = xpos.len() as f64;
    let lypos = ypos.len() as f64;

    if lx != lxpos {
        panic!("Input x and xpos have different lengths!");
    }
    if ly != lypos {
        panic!("Input y and ypos have different lengths!");
    }

    // let distances: Vec<i32> = match metric {
    //     // Match a single value
    //     "euclidean" => { 
    //         product(&xpos, &ypos)
    //             .map(|(a, b)| (a - b).abs())
    //             .collect::<Vec<i32>>()
    //     },
    //     "periodic" => {
    //         product(&xpos, &ypos)
    //             .map(|(a, b)| peuclidean(a,b,l))
    //             .collect::<Vec<i32>>()
    //     },
    //     _ => panic!("Not a valid distance metric is provided")
    // };
    // let distances = Array2::from_shape_vec((xpos.len(), ypos.len()), distances).unwrap();

    // let distances = product(&xpos, &ypos)
    //     .map(|(a, b)| (a - b).abs())
    //     .collect::<Vec<i32>>();
    // println!("distances: {:?}", distances);
    // for elem in distances.indexed_iter() {
    //     println!("{:?}", elem);
    // }

    let distances = get_dist_matrix(xpos, ypos, metric, l);
    let distances_all: Vec<i32> = (0..max_dist).collect();
    let ret = distances_all.par_iter()
        .map(|idist| 
    {
        let mut mi = 0.0;
        let mut mk = 0.0;
        let mut mimk = 0.0;
        let mut misquared = 0.0;
        let mut mksquared = 0.0;
        let mut n = 0.0;
        'inner: for elem in distances.indexed_iter() {
            let sdist = elem.1;
            // println!("chosen dist: {:?}", sdist);
            if (*sdist) == *idist {
                let i = elem.0.0;  
                let k = elem.0.1; 
                if i > k {continue 'inner;}

                mi += x[i];
                mk += y[k];
                mimk += x[i] * y[k];
                misquared += x[i].powi(2);
                mksquared += y[k].powi(2);
                n += 1.0;
            }
        }
        let mx = mi/n;
        let my = mk/n;
        let sx = misquared/n - mx.powi(2);
        let sy = mksquared/n - my.powi(2);
        (mimk/n - mx * my)/(sx * sy).sqrt()
    }
    ).collect();


    // //debug
    // println!("n: {:?}", n);
    // println!("mi: {:?}", mi);
    // println!("mk: {:?}", mk);
    // println!("mimk: {:?}", mimk);

    return ret;
}


#[extendr]
pub fn conn_crosscor_inhomo_ordered(x: &[f64], y: &[f64], xpos: &[i32], ypos: &[i32], max_dist: i32, metric: &str, l: i32) -> Vec<f64> {
    let lx = x.len() as f64;
    let ly = y.len() as f64;
    let lxpos = xpos.len() as f64;
    let lypos = ypos.len() as f64;

    if lx != lxpos {
        panic!("Input x and xpos have different lengths!");
    }
    if ly != lypos {
        panic!("Input y and ypos have different lengths!");
    }

    if metric != "euclidean" {
        panic!("Metric other than euclidean are not yet implmented!");
    }

    // let distances = get_dist_matrix(xpos, ypos, max_dist, metric, l);
    let len = max_dist as usize;
    let mut cor: Vec<f64> = vec![0.0; len];
    let mut mimk: Vec<f64> = vec![0.0; len];
    let mut mi: Vec<f64> = vec![0.0; len];
    let mut mk: Vec<f64> = vec![0.0; len];
    let mut misquared: Vec<f64> = vec![0.0; len];
    let mut mksquared: Vec<f64> = vec![0.0; len];
    let mut n: Vec<f64> = vec![0.0; len];
    for i in 0..xpos.len() {
        let mut k = i;
        'inner: while k < ypos.len() {
            // let sdist = *elem.1 as usize;
            // let sdist = match metric {
            //     "euclidean" => (xpos[i] - ypos[k]).abs() as usize,
            //     "periodic" => peuclidean(&xpos[i], &ypos[k],l) as usize,
            //     _ => todo!()
            // };

            let sdist = (xpos[i] - ypos[k]).abs() as usize;
            // println!("chosen dist: {:?}", sdist);
            // let i = elem.0.0;  
            // let k = elem.0.1; 
            if sdist >= (max_dist as usize) {break 'inner;}

            mi[sdist] += x[i];
            mk[sdist] += y[k];
            mimk[sdist] += x[i] * y[k];
            misquared[sdist] += x[i].powi(2);
            mksquared[sdist] += y[k].powi(2);
            n[sdist] += 1.0;
            k += 1;
        }
    }

    for k in (0..cor.len()).into_iter() {
        let sn = n[k];
        let mx = mi[k]/sn;
        let my = mk[k]/sn;
        let sx = misquared[k]/sn - mx.powi(2);
        let sy = mksquared[k]/sn - my.powi(2);
        cor[k] = (mimk[k]/sn - mx * my)/(sx * sy).sqrt();
        // println!("n: {:?}, mi: {:?}, mk: {:?}, mimk: {:?}, sx: {:?}, sy: {:?}", sn, mx, my, mimk[k], sx, sy);
    }

    return cor;
}

#[extendr]
fn get_density_field(x: &[f64], linear_size: f64, r: f64) -> Vec<f64> {
    let n_particles: usize = x.len();
    let dist = get_dist_matrix(x, x, "periodic_signed", linear_size);
    // println!("{:?}", dist);

    // let mut ret = [[0.0; 2]; N_PARTICLES];
    let mut ret_rho_grad = vec![0.0; n_particles];
    let mut ret_rho = vec![0.0; n_particles];
    for (k, row) in dist.axis_iter(Axis(0)).enumerate() {
        let n_left: f64 = row 
            .map(|x: &f64| ((*x > 0.0) & (*x <= r)) as u32)
            .into_iter()
            .sum::<u32>() as f64;
        // println!("L: {:?}", n_left);
        let n_right: f64 = row 
            .map(|x: &f64| ((*x < 0.0) & (*x >= { -r })) as u32)
            .into_iter()
            .sum::<u32>() as f64;
        // println!("R: {:?}", n_right);
        ret_rho[k] = (n_right + n_left + 1.0) / (2.0 * r) as f64;
        ret_rho_grad[k] = ((n_right/r) - (n_left/r)) / (r) as f64;
    }
    // (ret_rho, ret_rho_grad)
    ret_rho
}

#[extendr]
fn get_density_grad(x: &[f64], linear_size: f64, r: f64) -> Vec<f64> {
    let n_particles: usize = x.len();
    let dist = get_dist_matrix(x, x, "periodic_signed", linear_size);
    // println!("{:?}", dist);

    // let mut ret = [[0.0; 2]; N_PARTICLES];
    let mut ret_rho_grad = vec![0.0; n_particles];
    let mut ret_rho = vec![0.0; n_particles];
    for (k, row) in dist.axis_iter(Axis(0)).enumerate() {
        let n_left: f64 = row 
            .map(|x: &f64| ((*x > 0.0) & (*x <= r)) as u32)
            .into_iter()
            .sum::<u32>() as f64;
        // println!("L: {:?}", n_left);
        let n_right: f64 = row 
            .map(|x: &f64| ((*x < 0.0) & (*x >= { -r })) as u32)
            .into_iter()
            .sum::<u32>() as f64;
        // println!("R: {:?}", n_right);
        ret_rho[k] = (n_right + n_left + 1.0) / (2.0 * r) as f64;
        ret_rho_grad[k] = ((n_right/r) - (n_left/r)) / (r) as f64;
    }
    // (ret_rho, ret_rho_grad)
    ret_rho_grad
}


#[extendr]
fn get_velocity_field(x: &[f64], v: &[f64], linear_size: f64, r: f64) -> Vec<f64> {
    let n_particles: usize = x.len();
    let dist = get_dist_matrix(x, x, "periodic_signed", linear_size);

    let mut ret_v_grad = vec![0.0; n_particles];
    let mut ret_v = vec![0.0; n_particles];
    for (k, row) in dist.axis_iter(Axis(0)).enumerate() {
        let n_left: f64 = row 
            .map(|x: &f64| ((*x > 0.0) & (*x <= r)) as u32)
            .into_iter()
            .sum::<u32>() as f64;
        let n_right: f64 = row 
            .map(|x: &f64| ((*x < 0.0) & (*x >= { -r })) as u32)
            .into_iter()
            .sum::<u32>() as f64;
        let v_left: f64 = row.iter().zip(v.iter()).map(|(x,y)| {
            if *x <= r && *x > 0.0 {
                *y
            } else {
                0.0
            }
        }).sum::<f64>();
        let v_right: f64 = row.iter().zip(v.iter()).map(|(x,y)| {
            if *x >= -r && *x < 0.0 {
                *y
            } else {
                0.0
            }
        }).sum::<f64>();
        let vr_mean: f64 = if n_right > 1e-4 {
            v_right / n_right
        } else {
            0.0
        };
        let vl_mean: f64 = if n_left > 1e-4 {
            v_left / n_left 
        } else {
            0.0
        };
        ret_v[k] = (v_right + v_left + v[k]) / (n_left + n_right + 1.0);
        ret_v_grad[k] = (vr_mean - vl_mean) / (r);
    }
    ret_v
}


#[extendr]
pub fn rget_local_environment(xpos: &[f64], metric: &str, l: f64, r: f64) -> Robj {
    let ret = get_local_environments(xpos, metric, l, r);
    let ret = Robj::try_from(&ret).unwrap();
    return ret;
}
fn get_local_environments(x: &[f64], metric: &str, linear_size: f64, r: f64) -> Array2<f64> {
    let n_particles: usize = x.len();
    let dist = get_dist_matrix(x, x, metric, linear_size);

    let mut ret = Array2::<f64>::zeros((n_particles, 21)); 
    for i in 0..21 {
        for (k, row) in dist.axis_iter(Axis(0)).enumerate() {
            let row: Vec<i32> = row.into_iter().map(|&x| x as i32).collect::<Vec<i32>>().clone();
            let mut env = row 
                .into_iter()
                .filter(|&x| (x).abs() <= (r as i32))
                .collect::<Vec<i32>>().clone();
            env.sort_by_key(|&k| k.abs());
            // println!("GHere: {:?}", env);
            ret[[k,i]] = if i >= env.len() {
                -100.0
            } else {
                    env[i].into()
                };
        }
    }
    ret
}


/// @title Get an iterator over the cartesian product of two arrays
///
/// @examples
/// ```{rust}
/// let x = vec!['1', '2', '3'];
/// let y = vec!['a', 'b', 'c'];
/// let result: Vec<String> = product(&x, &y)
/// .map(|(a, b)| format!("{}{}", a, b))
/// .collect();
/// println!("{:?}", result)
/// ```
///
/// @export
fn product<'a: 'c, 'b: 'c, 'c, T>(
    xs: &'a [T],
    ys: &'b [T],
) -> impl Iterator<Item = (&'a T, &'b T)> + 'c {
    xs.iter().flat_map(move |x| std::iter::repeat(x).zip(ys))
}



/// @title distance matrix of a vector of positions
///
/// @examples
/// ```{r}
/// l <- 100
/// x <- sample(1:l, size = as.integer(l/10), replace = T)
/// get_dist_matrix_r(x, x, "euclidean", as.integer(l))
/// ```
/// @export
#[extendr]
pub fn Rget_dist_matrix(xpos: &[i32], ypos: &[i32], metric: &str, l: i32) -> Robj {
    let ret = get_dist_matrix(xpos, ypos, metric, l);
    let ret = Robj::try_from(&ret).unwrap();
    return ret;
}

pub fn get_dist_matrix<T>(xpos: &[T], ypos: &[T], metric: &str, l: T) -> Array2<T> 
where 
    T: NumericOps,
    T: Copy,
    T: std::ops::Sub<Output=T>,
    T: std::cmp::PartialOrd,
    T: std::ops::Mul<Output=T>
{
    let distances: Vec<T> = match metric {
        // Match a single value
        "euclidean" => { 
            product(&xpos, &ypos)
                .map(|(a, b)| (*a - *b).abs())
                .collect::<Vec<T>>()
        },
        "periodic" => {
            product(&xpos, &ypos)
                .map(|(a, b)| peuclidean(a,b, &l, false))
                .collect::<Vec<T>>()
        },
        "periodic_signed" => {
            product(&xpos, &ypos)
                .map(|(a, b)| peuclidean(a,b,&l, true))
                .collect::<Vec<T>>()
        },
        _ => panic!("Not a valid distance metric is provided")
    };
    let distances = Array2::from_shape_vec((xpos.len(), ypos.len()), distances).unwrap();
    return distances;
}

#[cfg(test)]
mod tests {
    // use crate::peuclidean;
    use rand::Rng;
    use crate::*;

    #[test]
    fn test_conn_crosscor_inhomo() {
        let n = 100;
        let xpos: Vec<i32> = (1..n).into_iter().map(|_| rand::thread_rng().gen_range(0..(2*n))).collect();
        let xpos: Vec<i32> = xpos.into_iter().collect();
        let x: Vec<f64> = (0..(xpos.len())).into_iter().map(|_| rand::thread_rng().gen_range(0.0..2.0)).collect();
        println!("x: {:?}\nxpos: {:?}", x, xpos);
        let res = conn_crosscor_inhomo_nonordered(&x, &x, &xpos, &xpos, 1000, "euclidean", 0);
        println!("res: {:?}", res);

        let res = conn_crosscor_inhomo_nonordered(&x, &x, &xpos, &xpos, 1000, "periodic", 2*n);
        println!("res: {:?}", res);
    }
    #[test]
    fn test_conn_crosscor_inhomo_new() {
        let n = 100;
        let xpos: Vec<i32> = (1..n).into_iter().map(|_| rand::thread_rng().gen_range(0..(2*n))).collect();
        let xpos: Vec<i32> = xpos.into_iter().collect();
        let x: Vec<f64> = (0..(xpos.len())).into_iter().map(|_| rand::thread_rng().gen_range(0.0..2.0)).collect();
        println!("x: {:?}\nxpos: {:?}", x, xpos);
        let res = conn_crosscor_inhomo_ordered(&x, &x, &xpos, &xpos, 20, "euclidean", 0);
        println!("res: {:?}", res);
    }
    #[test]
    fn test_peuclidean() {
        let mut rng = rand::thread_rng();
        for _ in 1..1000 {
            let r: f64= rng.gen_range(0.0..(10.0));
            let angle = rng.gen_range(0.0..(2.0*std::f64::consts::PI));
            let x_angle = rng.gen_range(0.0..(2.0*std::f64::consts::PI));
            let x = r * x_angle;
            let y = r * (x_angle + angle).rem_euclid(std::f64::consts::PI);
            let l = r * std::f64::consts::PI;
            assert!(peuclidean(&x, &y, &l, false) - r * angle <= f64::powi(10.0,-13));
        }
    }
    #[test]
    fn test_peuclidean_signed() {
            assert!(peuclidean(&2, &9, &10, true) == 3);
            assert!(peuclidean(&9, &2, &10, true) == -3);
    }
}
