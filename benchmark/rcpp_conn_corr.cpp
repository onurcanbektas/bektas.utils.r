#include <Rcpp.h>
using namespace Rcpp;

// [[Rcpp::export]]
NumericVector corr_fun_cpp(IntegerVector x, NumericVector m, int dist_max) {
  int L = x.length();
  NumericVector mimj(dist_max), mi(dist_max), mj(dist_max), cor(dist_max), misq(dist_max), mjsq(dist_max);
  IntegerVector n(dist_max);
  for(int i = 0; i <dist_max; i++){
    mimj(i)=0;
    mi(i)=0;
    mj(i)=0;
    n(i)=0;
    cor(i)=0;
    misq(i)=0;
    mjsq(i)=0;
  }
  for(int i=0; i<L;i++){
    int j=i;
    while(j < L){
      int dist = abs(x[j] - x[i]);
      if (dist > dist_max)
        break;
      mimj[dist] = mimj[dist]+ m[i]*m[j];
      mi[dist] += m[i];
      mj[dist] += m[j];
      n[dist]++;
      misq[dist] += m[i]*m[i];
      mjsq[dist] += m[j]*m[j];
      j++;
    }
  }
  for(int k = 0; k < dist_max; k ++){
    cor[k] = ((mimj[k]/n[k]) - (mi[k]/n[k])*(mj[k]/n[k])) / sqrt(( (misq[k]/n[k])-((mi[k]/n[k])*(mi[k]/n[k]))) * ((mjsq[k]/n[k]) - ((mj[k]/n[k])*(mj[k]/n[k]))));
    // printf("%d ", n[k]);
  }
  return(cor);
}
